import sys
fn = sys.argv[-1]

NEW = []
short_title = ''
affiliations = ''
correspondence = ''
with open(fn) as f:
    content = f.readlines()
    iline = 0
    line, processed = content[iline], False
    while (line!='* =preamble= :noexport:\n') and (iline<len(content)):
        line, processed = content[iline], False
        new_line = ''
        # if COMMENT
        if (line[:2]=='# '):
            pass # this is a comment
        # IF TITLE
        elif (line[:2]=='* '):
            NEW.append(line.replace('* ', '#+TITLE: '))
        # IF SHORT_TITLE
        elif len(line.split('Short Title: '))>1 and not processed:
            short_title = line.replace('Short Title: ', '')
        # IF AUTHORS
        elif len(line.split('Authors: '))>1:
            NEW.append('#+AUTHOR: '+line.split('Authors: ')[1].replace('}', '}$').replace('{', '$^{'))
        # IF AFFFILIATIONS
        elif len(line.split('Affiliations: '))>1:
            affiliations = line.split('Affiliations: ')[1]
        # If Correspondence
        elif len(line.split('Correspondence: '))>1:
            correspondence = line.split('Correspondence: ')[1]
            
        # IF SECTIONS
        elif (line=='** Key Points Summary\n'):
            NEW.append('\\normalsize \\bfseries ')
            NEW.append('* Key Points Summary\n')
            NEW.append('<<sec:key>>')
        elif (line=='** Significance statement\n'):
            NEW.append('\\normalsize \\bfseries ')
            NEW.append('* Significance statement\n')
            NEW.append('<<sec:significance>>')
        elif (line=='** Abstract\n'):
            NEW.append('\\normalsize \\bfseries ')
            NEW.append('* Abstract\n')
            NEW.append('<<sec:abstract>>')
        elif (line=='** Results\n'):
            NEW.append('\\normalsize \\normalfont')
            NEW.append('* Results\n')
            NEW.append('<<sec:results>>')
        elif (line=='** Introduction\n'):
            NEW.append('\\normalsize \\normalfont')
            NEW.append('* Introduction\n')
            NEW.append('<<sec:results>>')
        elif (line=='** Introduction\n'):
            NEW.append('\\normalsize \\normalfont')
            NEW.append('* Introduction\n')
            NEW.append('<<sec:intro>>')
        elif (line=='** Discussion\n'):
            NEW.append('\\normalsize \\normalfont')
            NEW.append('* Discussion\n')
            NEW.append('<<sec:discussion>>')
        elif (line=='** Material and Methods\n'):
            NEW.append('\small \\normalfont')
            NEW.append('* Material and Methods\n')
            NEW.append('<<sec:methods>>')
        elif (line=='** References\n'):
            NEW.append('\small \\normalfont')
            NEW.append('* References\n')
            NEW.append('<<sec:methods>>')
            
        # IF FIGURE
        elif (line[:11]=='*** FIGURE:'):
            caption_title = line.replace('*** FIGURE: ', '').split(':')[0].replace('\n', '')
            iline+=1 # we progress in the lines
            next_line = content[iline]
            if len(next_line.split('#+options'))>1: # meaning it is an option line
                if 'multicolumn' in next_line.replace('\n','').split(' : '):
                    NEW.append('#+ATTR_LATEX: :float multicolumn \n')
                # the first remaining key is the Figure ID
                NEW.append('#+NAME: '+next_line.split(' : ')[1]+'\n')
                iline+=1 # we progress in the lines
                next_line = content[iline]
                
            detailed_caption = next_line # we progress in the lines
            NEW.append('#+CAPTION: *'+caption_title+'* '+detailed_caption)
            iline+=1 # we progress in the lines
            figure_location = content[iline].replace('[[','').replace(']]','') # we progress in the lines
            NEW.append(content[iline])
            
        # IF SUBSECTIONS
        elif (line[:4]=='*** '):
            NEW.append(line.replace('** ', '*** '))
            subtitle = line.replace('*** ', '').split(' ')
            new_subtitle = subtitle[0]
            for i in range(min([3,len(subtitle)])):
                new_subtitle += '_'+subtitle[i]
            NEW.append('<<sec:'+new_subtitle+'>>\n')

            
        else:
            NEW.append(line)
            
        # if new_line!='':
        # else:
        #     NEW.append(line)
        iline+=1
        

for i, l in enumerate(NEW[:50]):
    print(i, l)

