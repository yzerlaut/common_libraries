cd ~ # go to home folder

# ===========================
# configure git
# -------------
echo 'Do you want to configure git ? y/[n]'
read yes_no
if [ "$yes_no" == 'y' ]
then
    echo "git settings >>  yann.zerlaut@gmail.com ; Yann Zerlaut"
    git config --global user.email "yann.zerlaut@gmail.com"
    git config --global user.name "Yann Zerlaut"
fi

# ===========================
# configure ssh
# -------------
echo 'Do you want to generate a new ssh key ? y/[n]'
read yes_no
if [ "$yes_no" == 'y' ]
then 
    ssh-keygen -t rsa -b 4096 -C "yann.zerlaut@gmail.com"
    eval "$(ssh-agent -s)"
    ssh-add -K ~/.ssh/id_rsa
    cat ~/.ssh/id_rsa.pub
    read -p "[...] =========== >> Copy the following key in the list of github ssh keys [Press anykey to continue]" none
fi

# ===========================
# configure emacs
# -------------
echo 'Do you want to install emacs ? y/[n]'
read yes_no
if [ "$yes_no" == 'y' ]
then 
    #install emacs
    echo "[...] installing emacs25"
    sudo add-apt-repository ppa:kelleyk/emacs
    sudo apt-get update
    sudo apt-get install emacs25
fi

# ===========================
# Cloning the RTXI repository
# -------------
echo 'Do you need to clone the **custom** rtxi repository ? y/[n]'
read yes_no
if [ "$yes_no" == 'y' ]
then 
    git clone git@github.com:yzerlaut/rtxi.git
fi

# ===========================
# Cloning the configurations repository
# -------------
echo 'Do you need to clone the *configs* repository ? y/[n]'
read yes_no
if [ "$yes_no" == 'y' ]
then 
    echo "[...] installing rtxi configs"
    cd ~
    git clone git@github.com:yzerlaut/my_rtxi_configs.git
    mv ~/my_rtxi_configs ~/configs
fi

# ===========================
# Installing rtxi
# -------------
echo 'Do you need to clone the *custom modules* for rtxi ? y/[n]'
read yes_no
if [ "$yes_no" == 'y' ]
then
    mkdir ~/modules
    cd ~/modules
    git clone git@github.com:yzerlaut/continuous-current-steps.git
    git clone git@github.com:yzerlaut/gamma-power-tracking.git
    git clone git@github.com:yzerlaut/in-vivo-like-cortical-act.git
    git clone git@github.com:yzerlaut/lif-model.git
    git clone git@github.com:yzerlaut/exc-inh-shotnoise-input.git
    cd
fi


# ===========================
# Installing rtxi
# -------------
echo 'Do you want to install the rtxi dependencies ? y/[n]'
read yes_no
if [ "$yes_no" == 'y' ]
then
    cd ~/rtxi/scripts
    echo '[...] installing dependencies'
    sudo apt-get install libssl-dev # needed
    sudo ./install_dependencies.sh
    cd ~
fi

echo 'Do you want to install the POSIX version of rtxi ? y/[n]'
read yes_no
if [ "$yes_no" == 'y' ]
then
    cd ~/rtxi/scripts
    echo '[...] installing dependencies'
    sudo ./install_rtxi_posix_no_modules.sh
    cd ~
fi

echo 'Do you want to install the real-time kernel ? y/[n]'
read yes_no
if [ "$yes_no" == 'y' ]
then
    cd ~/rtxi/scripts
    echo '[...] installing the RT kernel'
    sudo ./install_rt_kernel.sh
    cd ~
fi

echo 'Do you want to install rtxi ? y/[n]'
read yes_no
if [ "$yes_no" == 'y' ]
then
    cd ~/rtxi/scripts
    echo '[...] installing RTXI'
    sudo ./install_rtxi.sh
    cd ~
fi

# ===========================
# [in case of VM] configure the Virtual-Machine additions
# -------------
echo 'Do you need the Virtual-Machine additions ? y/[n]'
read yes_no
if [ "$yes_no" == 'y' ]
then 
    sudo apt-get install virtualbox-guest-dkms virtualbox-guest-utils virtualbox-guest-x11 
fi

# ===========================
# [in case of VM] configure the Virtual-Machine additions
# -------------
echo 'Do you want to update your bash profile? y/[n]'
read yes_no
if [ "$yes_no" == 'y' ]
then
    cp ~/common_libraries/bash/bash_profile.sh .bash_profile
fi


