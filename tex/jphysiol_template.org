- _*Affiliation*_ : $^{1}$ Unité de Neurosciences, Information et
  Complexité. Centre National de la Recherche Scientifique,
  FRE 3693. 1 Avenue de la Terrasse, 91198, Gif-sur-Yvette, France

- _*Running title*_ : A template for scientific papers based on =Org-Mode= and \LaTeX 

- _*Keywords*_ : Scientific writing, =Org-Mode=, \LaTeX

- _*Corresponding Author*_ : Yann Zerlaut,
  =yann.zerlaut@gmail.com=. Unité de Neurosciences, Information et
  Complexité. Centre National de la Recherche Scientifique,
  FRE 3693. 1 Avenue de la Terrasse, 91198, Gif-sur-Yvette, France

- _*Table of Contents category*_: Neuroscience – cellular/molecular 

# ================================================================ #
# For line numbering, you still need to include within the main org
# file, the \begin{linenumbers} [...] \end{linenumbers}
# ================================================================ #

#+LATEX_CLASS: article
#+OPTIONS: toc:nil (no Table Of COntents at all)
#+LaTeX_CLASS_OPTIONS: [8pt, colorlinks, a4paper]
#+LaTeX_HEADER:\usepackage{graphicx}
#+LaTeX_HEADER:\usepackage[AUTO]{inputenc}
#+LaTeX_HEADER:\usepackage[T1]{fontenc}
#+LaTeX_HEADER:\usepackage{lmodern}
#+LaTeX_HEADER:\usepackage{amsmath}
#+LaTeX_HEADER:\usepackage{microtype} % Slightly tweak font spacing for aesthetics
#+LaTeX_HEADER: \usepackage{geometry}
#+LaTeX_HEADER: \geometry{a4paper,total={210mm,297mm}, left=25mm, right=20mm, top=20mm, bottom=20mm, bindingoffset=0mm}
#+LaTeX_HEADER: \hypersetup{allcolors = gray}
#+LaTeX_HEADER: \renewcommand\thesection{}
#+LaTeX_HEADER: \renewcommand\thesubsection{}
#+LaTeX_HEADER: \usepackage{setspace, caption}
#+LaTeX_HEADER: \doublespacing
#+LaTeX_HEADER: \captionsetup{font=doublespacing}% Double-spaced float captions
#+LaTeX_HEADER: \renewcommand\ref{}
#+LaTeX_HEADER: \renewcommand{\refname}{\vspace{-.8cm}}
#+LaTeX_HEADER: \usepackage{lineno}
# #+LaTeX_HEADER: \renewcommand{\includegraphics}[2][]{\fbox{#2}}
#+LaTeX_HEADER: \usepackage[figuresonly, nolists]{endfloat}
#+LaTeX_HEADER: \usepackage{jneurosci}
#+LaTeX_HEADER: \bibliographystyle{jneurosci}

\linenumbers



