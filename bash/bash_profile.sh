server_path='/home/safaai/yann'
laptop_path='/Users/yzerlaut'

export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8


if [[ "$OSTYPE" == "darwin"* ]]; then
   # ---------- INKSCAPE
   alias inkscape='/Applications/Inkscape.app/Contents/MacOS/Inkscape'
   # ---------- EMACS
   alias emacs='/Applications/Emacs.app/Contents/MacOS/Emacs'
   alias em='/Applications/Emacs.app/Contents/MacOS/Emacs -nw -q'
elif [[ "$OSTYPE" == "linux-gnu" ]]; then
   alias em='emacs -nw -q'
fi
   
reload_scripts() {
source $HOME/work/common_libraries/bash/python_shortcuts.sh
source $HOME/work/common_libraries/bash/org_shortcuts.sh
source $HOME/work/common_libraries/bash/git_shortcuts.sh
source $HOME/work/common_libraries/bash/server_shortcuts.sh
source $HOME/work/common_libraries/bash/general_shortcuts.sh
source $HOME/work/common_libraries/bash/screen_shortcuts.sh
source $HOME/work/common_libraries/bash/new_repo_init.sh
}
alias reload=reload_scripts
reload # use it whenever you have updated one of those files
