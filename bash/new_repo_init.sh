create_repo_func() {
    # CREATE A NEW REPOSITORY WITH THE SUBMODULE DEPENDENCY
    
    # getting all informations
    echo 'Repository name?'
    read reponame
    echo 'Username [on bitbucket]?'
    read username
    echo 'Password [on bitbucket]?'
    read -s password  # -s flag hides password text

    echo "Creating the remote repository [...]"
    curl --user $username:$password https://api.bitbucket.org/1.0/repositories/ --data name=$reponame --data is_private='true'
    # other solution, NEEDS the bitbucket API, get it through: pip install bitbucket-cli
    # (not working->) bitbucket create --private --protocol ssh --scm git $reponame

    echo "Creating the local repository [...]"
    mkdir $reponame
    cd $reponame
    git init
    git remote add origin git@bitbucket.org:$username/$reponame.git
    git submodule init

    # first commit 
    echo "contributor:  yann.zerlaut@iit.it>" >> README.md
    git add README.md
    git commit -m 'Initial commit with contributor'
    git push -u origin master

    ## GRAPHS ##
    echo 'Do you want the "graphs" submodule repository ? [y]/n'
    read graph
    if [ "$graph" != 'n' ]
       then 
	   echo "Adding the 'graphs' submodule repository [...]"
    	   git submodule add https://yzerlaut@bitbucket.org/$username/graphs.git graphs
    	   cd graphs
    	   git checkout master
    	   cd ..
    fi
    ## PAPER ##
    echo 'Do you want the "paper" submodule repository ? [y]/n'
    read paper
    if [ "$paper" != 'n' ]
       then 
	   echo "Adding the 'paper' submodule repository [...]"
    	   git submodule add https://yzerlaut@bitbucket.org/$username/paper.git paper
    	   cd paper
    	   git checkout master
    	   cd ..
    fi
    ## DATA ANALYSIS ##
    echo 'Do you want the "data_analysis" repository ? [y]/n'
    read da
    if [ "$da" != "n" ];
       then 
    	   git submodule add https://yzerlaut@bitbucket.org/$username/data_analysis.git data_analysis
    	   cd data_analysis
    	   git checkout master
    	   cd ..
    fi
    ## NEURAL NETWORK DYNAMICS ##
    echo 'Do you want the "neural_network_dynamics" repository ? [y]/n'
    read nnd
    if [ "$nnd" != "n" ];
       then 
    	   git submodule add https://yzerlaut@bitbucket.org/$username/neural_network_dynamics.git neural_network_dynamics
    	   cd neural_network_dynamics
    	   git checkout master
    	   cd ..
    fi
    git commit -a -m 'Commit with submodules'
    git push origin master
    
    git submodule update
}

alias create_repo=create_repo_func

# CLONE A NEW REPOSITORY WITH THE SUBMODULE DEPENDENCY
clone_repo_func() {
    mkdir $1
    git clone git@bitbucket.org:yzerlaut/$1.git
    cd $1
    git submodule init
    git submodule update
}

alias clone_repo=clone_repo_func
