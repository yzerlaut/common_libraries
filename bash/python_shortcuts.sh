emacsnotebook() {
    # start the server
    jupyter notebook &> /dev/null &
    emacs --eval "(progn (ein:notebooklist-open))" --eval "(load-theme 'material-light t)" &
}
alias ein=emacsnotebook

alias quitjupyter="kill $(pgrep jupyter)"
