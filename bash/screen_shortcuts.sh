##########################
## Use of Screen
##########################

create_screen() {
    spull # to be sure to have the same version on server !!
    current_dir=$(pwd)
    dir_on_server="${current_dir/$HOME/$server_path}"
    ssh -t $server_address "cd $dir_on_server ; screen -S "$1
}
alias sc=create_screen

create_screen_with_modules_on_master() {
    spull # to be sure to have the same version on server !!
    current_dir=$(pwd)
    dir_on_server="${current_dir/$HOME/$server_path}"
    ssh -t $server_address "cd $dir_on_server ; cd data_analysis/ ;  git fetch origin ;  git checkout origin/master ; cd ../ ; cd neural_network_dynamics/ ; git fetch origin ; git checkout origin/master; cd ../ ; cd graphs/ ; git fetch origin ; git checkout origin/master ; cd ../ ; screen -S "$1
}
alias sc_master=create_screen_with_modules_on_master

list_screen() {
    ssh -t $server_address "screen -ls ; exit"
}
alias scl=list_screen

connect_screen() {
    ssh -t $server_address 'screen -r '$1
}
alias scr=connect_screen
