import os

def run_command(argument, with_data_file=None):
    dir_on_server = os.path.abspath('.').replace('/Users/yzerlaut/work', '/home/safaai/yann')
    os.system("ssh safaai@10.231.128.22 -t \"source ~/.bashrc ; cd "+dir_on_server+\
              " ; /home/safaai/anaconda3/bin/python "+argument+" \" ")
    if with_data_file is not None:
        os.system("scp safaai@10.231.128.22:"+dir_on_server+"/"+with_data_file+" "+with_data_file)

if __name__=='__main__':
    run_command('')

