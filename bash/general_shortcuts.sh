### Shortcuts for plotting datafiles
# Three complexity levels

all_svg_to_png() {
for i in *.svg; do
    echo $i
    echo ${i/svg/png}
    convert $i ${i/svg/png}
done
}
       
plot_datafile() {
    # go to the current directory given the server path structure
    python -c "import matplotlib.pylab as plt; import sys ; sys.path.append('/Users/yzerlaut/work/common_libraries');from graphs.my_graph import set_plot; import numpy as np ; data = np.load('"$1"') ; exec(str(data['plot']));plt.show(block=False);input('Hit Enter To Close');plt.close()"
}
alias plot=plot_datafile

basic_plot_datafile() {
    # go to the current directory given the server path structure
    python -c "import matplotlib.pylab as plt; import sys ; sys.path.append('/Users/yzerlaut/work/common_libraries');from graphs.my_graph import set_plot; import numpy as np ; data = np.load('"$1"') ; exec(str(data['basic_plot']));plt.show(block=False);input('Hit Enter To Close');plt.close()"
}
alias basic_plot=basic_plot_datafile

fancy_plot_datafile() {
    # go to the current directory given the server path structure
    python -c "import matplotlib.pylab as plt; import sys ; sys.path.append('/Users/yzerlaut/work/common_libraries');from graphs.my_graph import set_plot; import numpy as np ; data = np.load('"$1"') ; exec(str(data['plot']));plt.show();fig.savefig('fig.svg')"
}
alias fancy_plot=fancy_plot_datafile

plot_datafile_to_png() {
    # go to the current directory given the server path structure
    python -c "import matplotlib.pylab as plt; import sys ; sys.path.append('/Users/yzerlaut/work/common_libraries');from graphs.my_graph import set_plot; import numpy as np ; data = np.load('"$1"') ; exec(str(data['plot']));fig.savefig('"$2"')"
}
alias plot_to_png=plot_datafile_to_png
    
plot_datafile_to_svg() {
    # go to the current directory given the server path structure
    python -c "import matplotlib.pylab as plt; import sys ; sys.path.append('/Users/yzerlaut/work/common_libraries');from graphs.my_graph import set_plot; import numpy as np ; data = np.load('"$1"') ; exec(str(data['plot']));fig.savefig('"$2"', format='svg')"
}
alias plot_to_svg=plot_datafile_to_svg

from_notebook_to_pdf() {
    # you need to have set 'hidecode.tplx' in a folder visible to nbconvert
    # in ~/.jupyter/jupyter_nbconvert_config.py:
    # c.TemplateExporter.template_path = ['.',\
    #               '/Users/yzerlaut/work/common_libraries/config_files']

    jupyter nbconvert --template notebook_pdf_export.tplx --to pdf $1
}
alias nb2pdf=from_notebook_to_pdf
