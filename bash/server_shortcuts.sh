server_address="safaai@10.231.128.22"
alias log="ssh $server_address"
alias logx="ssh -X $server_address"
alias cache_cred="git config --global credential.helper 'cache --timeout 31*24*3600'"

go_to_mirror_dir() {
    # go to the current directory given the server path structure
    current_dir=$(pwd)
    # then same directory in cluster
    dir_on_server="${current_dir/$laptop_path/$server_path}"
    ssh -t $server_address "cd $dir_on_server ; bash"
}
alias sdir=go_to_mirror_dir

copy_to_mirror_dir() {
    # go to the current directory given the server path structure
    current_dir=$(pwd)
    # then same directory in cluster
    dir_on_server="${current_dir/$laptop_path/$server_path}"
    scp $1 $server_address:$dir_on_server/$1
}
alias scopy=copy_to_mirror_dir

copy_dir_to_mirror_dir() {
    # go to the current directory given the server path structure
    current_dir=$(pwd)
    # then same directory in cluster
    dir_on_server="${current_dir/$laptop_path/$server_path}"
    scp -r "$1" $server_address:$dir_on_server/"$1"
}
export -f copy_dir_to_mirror_dir
alias scopydir=copy_dir_to_mirror_dir

get_from_mirror_dir() {
    # go to the current directory given the server path structure
    current_dir=$(pwd)
    server_address='safaai@10.231.128.22'
    # then same directory in cluster
    dir_on_server="${current_dir/$laptop_path/$server_path}"
    # echo $server_address:$dir_on_server/$1 $1
    scp "${server_address}":"${dir_on_server}"/"${1}" "${1}"
}
export -f get_from_mirror_dir
alias sget=get_from_mirror_dir

rsync_to_mirror_dir() {
    # go to the current directory given the server path structure
    current_dir=$(pwd)
    # then same directory in cluster
    dir_on_server="${current_dir/$laptop_path/$server_path}"
    rsync -ar $1 $server_address:$dir_on_server/$1
}

rsync_from_mirror_dir() {
    # go to the current directory given the server path structure
    current_dir=$(pwd)
    # then same directory in cluster
    dir_on_server="${current_dir/$laptop_path/$server_path}"
    rsync -ar $server_address:$dir_on_server/$1 $1
}

get_data_file_script() {
    # go to the current directory given the server path structure
    current_dir=$(pwd)
    # then same directory in cluster
    dir_on_server="${current_dir/$laptop_path/$server_path}"
    output_datafile=$dir_on_server'/data.h5'
    scp $server_address:$output_datafile ./
    DATE=$(date +%Y-%m-%d"-"%H:%M:%S).h5
    scp data.h5 $server_address:/mnt/tobackup/$DATE # making backup on server
    cp data.h5 /tmp/$DATE # making backup on laptop
}
alias sdata=get_data_file_script

get_data_file_folder() {
    # go to the current directory given the server path structure
    current_dir=$(pwd)
    # then same directory in cluster
    dir_on_server="${current_dir/$laptop_path/$server_path}"
    output_datafile=$dir_on_server'/data/'
    scp -r $server_address:$output_datafile ./
}
alias sdata_folder=get_data_file_folder

pull_on_server() {
    current_dir=$(pwd)
    git commit -a -m "==================== \n if commit works you forgot to commit !!! \n =========================="
    git push origin master
    dir_on_server="${current_dir/$laptop_path/$server_path}"
    ssh $server_address -t "source ~/.bashrc ; cd $dir_on_server ; git pull origin master ; git submodule update --recursive"
}
alias spull=pull_on_server

start_notebook() {
    # go to the current directory given the server path structure
    current_dir=$(pwd)
    # then same directory in cluster
    dir_on_server="${current_dir/$laptop_path/$server_path}"
    ssh -X $server_address -t "cd $dir_on_server ; source ~/.bashrc ; /home/safaai/anaconda3/bin/jupyter notebook --ip=10.231.128.22 --port=8889"
}
alias snb=start_notebook

run_script_func() {
    pull_on_server
    # getting extension and choosing program
    EXTENSION=`echo "$1" | cut -d'.' -f2`
    if [ $EXTENSION == 'py' ]
    then
	COMMAND='/home/safaai/anaconda3/bin/python '
    elif [ $EXTENSION == 'pynrn' ]
    then
	COMMAND='/usr/bin/python '
    elif [ $EXTENSION == 'sh' ]
    then
	COMMAND='bash '
    fi
    # adding the arguments
    for var in "$@"
    do
	COMMAND=$COMMAND" "$var
    done
    echo $COMMAND
    current_dir=$(pwd)
    # then same directory in cluster
    dir_on_server="${current_dir/$laptop_path/$server_path}"
    ssh $server_address -t "source ~/.bashrc ; cd $dir_on_server ; "$COMMAND
    get_data_file_script
}
alias srun=run_script_func
